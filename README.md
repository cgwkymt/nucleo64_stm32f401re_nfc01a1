# nucleo64_stm32f401re_nfc01a1

`STM32F401 Nucleo-64`と`X-NUCLEO-NFC01A1`のサンプルプロジェクト。

## 必要なもの

本プロジェクトをビルドし実行するためには、以下のハードウェアと開発ツールが必要。

### ハードウェア

- PC : Windows PCもしくは、Linux PC
- `NUCLEO-F401RE` : STM32F401REを搭載したマイコンボード
- `X-NUCLEO-NFC01A1` : NFCタグ(ISO 14443-4 TypeA)開発用拡張ボード
- USBケーブル (TypeA(オス)-miniB(オス)) : PCにマイコンボードを接続するためのケーブル

### 開発ツール

- `STM32CubeMX` `6.6.1`
  - https://www.st.com/ja/development-tools/stm32cubemx.html
- `STM32CubeIDE` `1.10.1`
  - https://www.st.com/ja/development-tools/stm32cubeide.html

## 使い方

本プロジェクトをビルドしてプログラムをマイコンボードに書き込むと、NFCタグとして機能する。

- `X-NUCLEO-NFC01A1`のLEDがそれぞれ以下の様になればNFCタグとして動作している。
  - `LED2` : 点灯 (青)
  - `LED3` : 点灯 (オレンジ)
- スマホなどでNFCリーダーアプリを立ち上げて、`X-NUCLEO-NFC01A1`にかざすと、タグに書かれたデータ"www.google.com/search?q=templature"を読むことができる。

## 開発手順

`STM32CubeMX`と`STM32CubeIDE`を使用した開発手順の流れは以下の通り。

1. `STM32CubeMX`でマイコンもしくはマイコンボードのハードウェアの設定を行う。
2. `STM32CubeMX`でソースコードを自動生成する。
3. `STM32CubeIDE`で自動生成されたソースコードを編集する。
4. `STM32CubeIDE`でビルドを実行しプログラムを生成する。
5. `STM32CubeIDE`でマイコンボードにプログラムを転送する。必要に応じて `STM32CubeIDE`上でソースコードデバッグを行う。
6. マイコンボードの電源を入れ直してマイコンボードに書き込んだ新しいプログラムを実行する。

### `STM32CubeMX`での作業

#### 新規プロジェクトの作成

`STM32CubeMX`を起動し、新規プロジェクトを作成する。

1. `File` -> `New Project`を選択。
2. `Board Selector`タブを選択。
3. `NUCLEO-F401RE`を選択。
4. 右上の`Start Project`を押下。

#### プロジェクトの設定

`STM32CubeMX`で作成したプロジェクトの設定を行う。

1. PIN設定
   1. `Pinout & Configuration`タブを選択。
   2. PIN設定を行う。
2. クロック設定
   1. `Clock Configuration`タブを選択。
   2. クロック設定を行う。(必要に応じて)
3. プロジェクト設定
   1. `Project Manager`タブを選択。
   2. 左側の`Project`を選択し、以下の設定を行う。
      - `Project Name`欄にプロジェクト名を設定。
         - `Toolchain/IDE`から`STM32CubeIDE`を選択。
         - `Generate Under Root`にチェックをつける。
         - `Minimum Heap Size` = `0x200`
         - `Minimum Stack Size` = `0x200`
      1. 左側の`Code Generator`を選択し、以下の設定を行う。
         - `STM32Cube MCU packages and embedded software packs`
           - `Copy only the necessary library files`を選択。
         - `Generated files`
           - `Backup previously generated files when re-generating`のチェックを外す。それ以外のチェックをつける。
         - `HAL settings`
           - すべてチェックを外す。

#### コードの自動生成

右上の`GENERATE CODE`を押下し、ソースコードを生成する。

#### プロジェクトの保存と`STM32CubeMX`の終了

`File` -> `Save Project`を選択し、プロジェクトを保存する。

`File` -> `Exit`を選択し、`STM32CubeMX`を閉じる。

### `STM32CubeIDE`での作業

#### ソースコードの修正

プログラムの動作を変更するには、`STM32CubeMX`で自動生成された`Core/src/main.c`を適宜修正する。

本プロジェクトでは、自動生成されたソースコードに`STM32Cube expansion software for X-NUCLEO-NFC01A1` ("https://www.st.com/ja/embedded-software/x-cube-nfc1.html") を組み込んで、`Core/src/main.c`を修正して`X-NUCLEO-NFC01A1`の制御を追加している。

#### プロジェクトビルド

`STM32CubeIDE`を起動しプロジェクトのビルドを行う。

1. `File` -> `Open Projects from File System`を選択。
   1. `Directory`を押下し`STM32CubeMX`で作成したプロジェクトのディレクトリを選択。
   2. `Finish`ボタンを押下。
2. `Project` -> `Build Project`を押下しビルドを実行。

#### プログラムの実行

マイコンボードにプログラムを転送し、マイコンボード上でプログラムを実行する。

1. `Run` -> `Debug`を押下し、`STM32CubeIDE`をデバッグモードに変更する。
2. `STM32CubeIDE`の`Console`に`Download verified successfully`と表示されるまで待つ。
3. `Run` -> `Resume`を選択し、マイコンボード上でプログラムを実行する。
